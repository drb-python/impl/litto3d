import numpy
import rasterio
from drb.drivers.file import DrbFileNode
from drb.drivers.litto3d import DrbLitto3dNode

path = "path/to/a/litto3d_file.asc"

base_node = DrbFileNode(path)
litto_node = DrbLitto3dNode(base_node)

# Get all the attributes names of the file node such as cols and rows
litto_node.attribute_names()

# Get the numbers of rows in the data array
litto_node @ "rows"

# Get the data as numpy array
litto_node.get_impl(numpy.ndarray)

# Get the data as a rasterio dataset
litto_node.get_impl(rasterio.DatasetReader)
