.. _example:

Examples
=========

Open and read a .asc/.xyz file
-----------------------------
.. literalinclude:: example/open.py
    :language: python
