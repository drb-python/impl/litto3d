===================
Data Request Broker
===================
---------------------------------
Litto3d driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-litto3d/month
    :litto3dget: https://pepy.tech/project/drb-driver-litto3d
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-litto3d.svg
    :litto3dget: https://pypi.org/project/drb-driver-litto3d/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-litto3d.svg
    :litto3dget: https://pypi.org/project/drb-driver-litto3d/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-litto3d.svg
    :litto3dget: https://pypi.org/project/drb-driver-litto3d/
    :alt: Python Version Support Badge

-------------------

This drb-driver-litto3d module implements access to litto3d format (.asc and .xyz files) with DRB data model.
It is able to retrieve common implementations such as pandas, numpy or rasterio.


User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

