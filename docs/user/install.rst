.. _install:

Installation of litto3D implementation
====================================
Installing ``drb-driver-litto3d`` with execute the following in a terminal:

.. code-block::

   pip install drb-driver-litto3d
